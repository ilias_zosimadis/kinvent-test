package github.io.kinvent.modelview;

import android.arch.lifecycle.ViewModel;

import java.util.ArrayList;

import github.io.kinvent.model.Recipe;
import github.io.kinvent.model.RecipeModel;
import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

public class RecipeViewModel extends ViewModel {

    private RecipeModel recipeModel;
    private final BehaviorSubject<String> currentRecipe = BehaviorSubject.create();

    public RecipeViewModel() {
        recipeModel = new RecipeModel();
    }

    /**
     * Get random recipe from your model.
     *
     * @return
     */
    public Observable<Recipe> getRandomRecipe() {
        return recipeModel.getRandomRecipe();
    }


    /**
     * Get all recipes from model.
     *
     * @return
     */
    public Observable<ArrayList<Recipe>> getRecipes() {
        return recipeModel.getRecipes();
    }

    /**
     * Pass the selected recipe to model.
     *
     * @param recipeName
     */
    public void setCurrentRecipe(String recipeName) {
        currentRecipe.onNext(recipeName);
    }

}

package github.io.kinvent.model.ingredients;

public class Tomato implements Iingridient {
    public int numberOfItems;

    public Tomato(int numberOfItems) {
        this.numberOfItems = numberOfItems;
    }

    @Override
    public void doSomething() {

    }

    @Override
    public String toString() {
        return "Tomato";
    }
}

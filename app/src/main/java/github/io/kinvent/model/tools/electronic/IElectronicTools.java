package github.io.kinvent.model.tools.electronic;

public interface IElectronicTools {

    void process();

    boolean hasEnergy();
}

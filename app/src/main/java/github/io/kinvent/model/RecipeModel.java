package github.io.kinvent.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import io.reactivex.Observable;

public class RecipeModel {

    private Map<String, Recipe> recipes;

    public RecipeModel() {
        recipes = RecipeData.getAllRecipes();
    }

    public Observable<ArrayList<Recipe>> getRecipes() {
        return Observable.just(new ArrayList<>(recipes.values()));
    }

    public Observable<Recipe> getRecipe(String recipeName) {
        return Observable.just(recipes.get(recipeName));
    }

    public Observable<Recipe> getRandomRecipe() {
        Random randomGen = new Random();
        List<Recipe> values = new ArrayList<>(recipes.values());

        return Observable.just(values.get(randomGen.nextInt(values.size())));
    }
}

package github.io.kinvent.model.ingredients;

public class Egg implements Iingridient {
    private int numberOfItems;

    public Egg(int numberOfItems) {
        this.numberOfItems = numberOfItems;
    }

    @Override
    public void doSomething() {

    }

    @Override
    public String toString() {
        return "Egg";
    }
}

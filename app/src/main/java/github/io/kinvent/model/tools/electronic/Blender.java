package github.io.kinvent.model.tools.electronic;

public class Blender implements IElectronicTools {
    private String name;

    public Blender(String name) {
        this.name = name;
    }

    @Override
    public void process() {
        //You can do something here
    }

    @Override
    public boolean hasEnergy() {
        return false;
    }

    @Override
    public String toString() {
        return "Blender";
    }
}

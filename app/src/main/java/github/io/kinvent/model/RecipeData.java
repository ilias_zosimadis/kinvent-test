package github.io.kinvent.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import github.io.kinvent.model.ingredients.Chicken;
import github.io.kinvent.model.ingredients.Egg;
import github.io.kinvent.model.ingredients.Iingridient;
import github.io.kinvent.model.ingredients.Tomato;
import github.io.kinvent.model.tools.electronic.IElectronicTools;
import github.io.kinvent.model.tools.electronic.Toaster;
import github.io.kinvent.model.tools.manual.IManualTools;
import github.io.kinvent.model.tools.manual.Ladle;

public class RecipeData {

    public static Map<String, Recipe> allRecipes = new HashMap<String, Recipe>();

    public static Map<String, Recipe> getAllRecipes() {
        if (allRecipes.isEmpty()) {
            loadRecipes();
        }

        return allRecipes;
    }

    /**
     * Loads hardcoded recipes for our application. You can replace
     * this helper method (and class) with Room/Sqlite etc.
     */
    private static void loadRecipes() {
        Recipe caesarSalad = new Recipe();

        List<Iingridient> caesarIngridients = new ArrayList<>();
        caesarIngridients.add(new Tomato(1));
        caesarIngridients.add(new Egg(1));
        caesarIngridients.add(new Chicken(1));

        List<IElectronicTools> caesarElectronicTools = new ArrayList<>();
        caesarElectronicTools.add(new Toaster("Bosch"));

        List<IManualTools> caesarManualTools = new ArrayList<>();
        caesarManualTools.add(new Ladle("Vickos"));

        caesarSalad.setNameOfRecipe("Caesar Salad");
        caesarSalad.setIngridientList(caesarIngridients);
        caesarSalad.setElectronicToolsList(caesarElectronicTools);
        caesarSalad.setManualToolsList(caesarManualTools);

        allRecipes.put("Caesar Salad", caesarSalad);
    }


}

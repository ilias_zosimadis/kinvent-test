package github.io.kinvent.model.ingredients;

public class Chicken implements Iingridient {
    public int numberOfItems;

    public Chicken(int numberOfItems) {
        this.numberOfItems = numberOfItems;
    }

    @Override
    public void doSomething() {
        //Do something here
    }

    @Override
    public String toString() {
        return "Chicken";
    }
}

package github.io.kinvent.model.tools.manual;

public class Mortar implements IManualTools {
    private String name;

    public Mortar(String name) {
        this.name = name;
    }

    @Override
    public void processManual() {

    }

    @Override
    public String toString() {
        return "Mortar";
    }
}

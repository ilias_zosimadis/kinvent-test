package github.io.kinvent.model.tools.manual;

public class Knife implements IManualTools {
    private String name;
    private String type;

    public Knife(String name, String type) {
        this.name = name;
        this.type = type;
    }

    @Override
    public void processManual() {

    }

    @Override
    public String toString() {
        return "Knife";
    }
}

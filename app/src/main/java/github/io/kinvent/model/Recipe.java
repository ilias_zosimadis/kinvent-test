package github.io.kinvent.model;

import java.util.ArrayList;
import java.util.List;

import github.io.kinvent.model.ingredients.Iingridient;
import github.io.kinvent.model.tools.electronic.IElectronicTools;
import github.io.kinvent.model.tools.manual.IManualTools;

public class Recipe {
    /**
     * The name of the recipe.
     */
    private String nameOfRecipe;

    /**
     * All ingredients of the recipe.
     */
    private List<Iingridient> ingredientList;

    /**
     * All electronic tools you need for the recipe.
     */
    private List<IElectronicTools> electronicToolsList;

    /**
     * All manual tools your need for the recipe.
     */
    private List<IManualTools> manualToolsList;


    public String getNameOfRecipe() {
        return nameOfRecipe;
    }

    public void setNameOfRecipe(String nameOfRecipe) {
        this.nameOfRecipe = nameOfRecipe;
    }

    public List<Iingridient> getIngredientList() {
        return ingredientList;
    }

    public List<String> getIngredientNames() {
        List<String> allIngridientsNames = new ArrayList<>();
        for (Iingridient ingredient : ingredientList) {
            allIngridientsNames.add(ingredient.toString());
        }

        return allIngridientsNames;
    }

    public void setIngridientList(List<Iingridient> ingredientList) {
        this.ingredientList = ingredientList;
    }

    public List<IElectronicTools> getElectronicToolsList() {
        return electronicToolsList;
    }


    public List<String> getElectronicToolsNames() {
        List<String> allElectronicsToolsNames = new ArrayList<>();
        for (IElectronicTools electronicTool : electronicToolsList) {
            allElectronicsToolsNames.add(electronicTool.toString());
        }

        return allElectronicsToolsNames;
    }

    public void setElectronicToolsList(List<IElectronicTools> electronicToolsList) {
        this.electronicToolsList = electronicToolsList;
    }

    public List<IManualTools> getManualToolsList() {
        return manualToolsList;
    }

    public List<String> getManualToolsNames() {
        List<String> allManualToolsNames = new ArrayList<>();
        for (IManualTools manualTool : manualToolsList) {
            allManualToolsNames.add(manualTool.toString());
        }

        return allManualToolsNames;
    }

    public void setManualToolsList(List<IManualTools> manualToolsList) {
        this.manualToolsList = manualToolsList;
    }
}

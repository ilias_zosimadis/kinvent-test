package github.io.kinvent.model.tools.manual;

public class Ladle implements IManualTools {
    private String name;

    public Ladle(String name) {
        this.name = name;
    }

    @Override
    public void processManual() {

    }

    @Override
    public String toString() {
        return "Ladle";
    }
}

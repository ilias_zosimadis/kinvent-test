package github.io.kinvent.model.tools.electronic;


public class Toaster implements IElectronicTools {
    private String name;

    public Toaster(String name) {
        this.name = name;
    }

    @Override
    public void process() {

    }

    @Override
    public boolean hasEnergy() {
        return false;
    }

    @Override
    public String toString() {
        return "Toaster";
    }
}

package github.io.kinvent.model;

public class Kitchen {

    private volatile static Kitchen kitchen;
    private boolean hasHotPlate = true;
    private boolean hasStove = true;
    private boolean hasSink = true;
    private int numberOfBench = 2;

    private Kitchen() {
    }

    /**
     * Get kitchen instance (handle singleton issues)
     * @return
     */
    public static Kitchen getInstance() {
        if (kitchen == null) {
            synchronized (Kitchen.class) {
                if (kitchen == null) {
                    kitchen = new Kitchen();
                }
            }
        }
        return kitchen;
    }


    public boolean hasHotPlate() {
        return hasHotPlate;
    }

    public boolean hasStove() {
        return hasStove;
    }

    public boolean hasSink() {
        return hasSink;
    }

    public int getNumberOfBench() {
        return numberOfBench;
    }
}

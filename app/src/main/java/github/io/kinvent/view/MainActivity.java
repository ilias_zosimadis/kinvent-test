package github.io.kinvent.view;

import android.arch.lifecycle.ViewModelProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import github.io.kinvent.R;
import github.io.kinvent.model.Kitchen;
import github.io.kinvent.model.Recipe;
import github.io.kinvent.modelview.RecipeViewModel;
import github.io.kinvent.view.listview.ListViewCustomAdapter;
import github.io.kinvent.view.listview.ListViewDTO;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    @NonNull
    private CompositeDisposable compositeDisposable;

    @NonNull
    private ListView recipeIngredientsListView;

    @NonNull
    private ListView recipeToolsListView;

    @NonNull
    private TextView recipeNameTextView;

    @NonNull
    private RecipeViewModel recipeViewModel;

    private Kitchen kitchen;

    private Recipe recipeToFind;

    private Set<String> userCheckedItems = new HashSet<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recipeIngredientsListView = findViewById(R.id.lvRecipeIngr);
        recipeToolsListView = findViewById(R.id.lvRecipeTools);
        recipeNameTextView = findViewById(R.id.tvRecipeName);

        kitchen = Kitchen.getInstance();

        recipeViewModel = ViewModelProvider.AndroidViewModelFactory.getInstance(getApplication()).create(RecipeViewModel.class);
        setItemClickListener();
    }

    /**
     * Checks if the player's answer is correct. In this version it works
     * only with tools and ingridients.
     *
     * @param view
     */
    public void checkAnswer(View view) {
        Set<String> recipeToFindSet = new HashSet<>(recipeToFind.getElectronicToolsNames());
        recipeToFindSet.addAll(recipeToFind.getManualToolsNames());
        recipeToFindSet.addAll(recipeToFind.getIngredientNames());

        if (userCheckedItems.equals(recipeToFindSet)) {
            Toast.makeText(this, "Correct", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Wrong", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Set listeners for the two listviews.
     */
    private void setItemClickListener() {
        recipeIngredientsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Object itemObject = adapterView.getAdapter().getItem(i);

                ListViewDTO item = (ListViewDTO) itemObject;
                CheckBox itemCheckBox = (CheckBox) view.findViewById(R.id.cb_list_view);

                if (item.isChecked()) {
                    userCheckedItems.remove(item.getItemText());
                    itemCheckBox.setChecked(false);
                    item.setChecked(false);
                } else {
                    userCheckedItems.add(item.getItemText());
                    itemCheckBox.setChecked(true);
                    item.setChecked(true);
                }

            }
        });

        recipeToolsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Object itemObject = adapterView.getAdapter().getItem(i);

                ListViewDTO item = (ListViewDTO) itemObject;
                CheckBox itemCheckBox = (CheckBox) view.findViewById(R.id.cb_list_view);

                if (item.isChecked()) {
                    userCheckedItems.remove(item.getItemText());
                    itemCheckBox.setChecked(false);
                    item.setChecked(false);
                } else {
                    userCheckedItems.add(item.getItemText());
                    itemCheckBox.setChecked(true);
                    item.setChecked(true);
                }

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        compositeDisposable = new CompositeDisposable();
        compositeDisposable.add(recipeViewModel.getRandomRecipe()
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::setRandomRecipe));

        compositeDisposable.add(recipeViewModel.getRecipes()
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::setRecipeSteps));

    }


    @Override
    protected void onPause() {
        compositeDisposable.clear();
        userCheckedItems.clear();

        super.onPause();
    }

    /**
     * Set data for ingredients and tools lists based on your
     * recipes. This way you will have new data for your lists
     * when you update your recipes.
     *
     * @param recipes all the recipes in your collection.
     */
    private void setRecipeSteps(ArrayList<Recipe> recipes) {
        List<String> allIngredients = new ArrayList<>();
        List<String> allTools = new ArrayList<>();

        for (Recipe rec : recipes) {
            allIngredients.addAll(rec.getIngredientNames());
            allTools.addAll(rec.getManualToolsNames());
            allTools.addAll(rec.getElectronicToolsNames());
        }

        List<ListViewDTO> listViewIngrDTO = this.setListViewDTO(allIngredients);
        List<ListViewDTO> listViewToolsDTO = this.setListViewDTO(allTools);

        ListViewCustomAdapter listViewIngrCustomAdapter = new ListViewCustomAdapter(getApplication(), listViewIngrDTO);
        listViewIngrCustomAdapter.notifyDataSetChanged();

        ListViewCustomAdapter listViewToolsCustomAdapter = new ListViewCustomAdapter(getApplication(), listViewToolsDTO);
        listViewToolsCustomAdapter.notifyDataSetChanged();

        recipeIngredientsListView.setAdapter(listViewIngrCustomAdapter);
        recipeToolsListView.setAdapter(listViewToolsCustomAdapter);

    }

    /**
     * Helper method for listview DTO. Not the best approach
     * to pass List<String>
     *
     * @param items the items for your listview.
     * @return
     */
    private List<ListViewDTO> setListViewDTO(List<String> items) {
        List<ListViewDTO> resultDTO = new ArrayList<>();

        for (String itemName : items) {
            ListViewDTO item = new ListViewDTO();
            item.setItemText(itemName);
            item.setChecked(false);

            resultDTO.add(item);
        }

        return resultDTO;
    }


    /**
     * Get a random recipe for the game.
     *
     * @param recipeToFind the recipe that the user has to find.
     */
    private void setRandomRecipe(Recipe recipeToFind) {
        this.recipeToFind = recipeToFind;
        recipeNameTextView.setText(recipeToFind.getNameOfRecipe());

    }


}

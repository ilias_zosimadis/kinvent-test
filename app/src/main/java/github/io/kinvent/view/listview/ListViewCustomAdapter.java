package github.io.kinvent.view.listview;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;

import github.io.kinvent.R;

public class ListViewCustomAdapter extends BaseAdapter {

    private List<ListViewDTO> listViewDTO = null;
    private Context context = null;

    public ListViewCustomAdapter(Context context, List<ListViewDTO> listViewDTO) {
        this.context = context;
        this.listViewDTO = listViewDTO;
    }

    @Override
    public int getCount() {
        int count = 0;
        if (listViewDTO != null) {
            count = listViewDTO.size();
        }
        return count;
    }

    @Override
    public Object getItem(int i) {
        Object item = null;
        if (listViewDTO != null) {
            item = listViewDTO.get(i);
        }

        return item;
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ListViewHolder viewHolder = null;

        if (view != null) {
            viewHolder = (ListViewHolder) view.getTag();
        } else {
            view = View.inflate(context, R.layout.list_view, null);
            CheckBox checkBox = view.findViewById(R.id.cb_list_view);
            TextView textView = view.findViewById(R.id.cb_list_view);

            textView.setTextColor(Color.BLACK); // handle this with your xml

            viewHolder = new ListViewHolder(view);

            viewHolder.setItemCheckBox(checkBox);
            viewHolder.setItemTextView(textView);
            view.setTag(viewHolder);

        }
        ListViewDTO item = listViewDTO.get(i);
        viewHolder.getItemCheckBox().setChecked(item.isChecked());
        viewHolder.getItemTextView().setText(item.getItemText());

        return view;
    }
}
